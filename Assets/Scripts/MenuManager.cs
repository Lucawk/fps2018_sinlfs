﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    private float logo = 0;
    private float fps = 20;
    // Start is called before the first frame update
    public void PulsaPlay()
    {
        SceneManager.LoadScene("MainMenu");
    }

        public void PulsaFps()
    {
        SceneManager.LoadScene("Fps");
    }

        public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

        public void PulsaCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }
}
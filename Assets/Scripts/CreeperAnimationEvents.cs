﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreeperAnimationEvents : MonoBehaviour {

    TreeBehaviour creeper;
    private void Start()
    {
        creeper = GetComponentInParent<TreeBehaviour>();
    }
    public void EndExplosion()
    {
        Debug.Log("Explosion!!!");
        creeper.Explosion();
    }
}

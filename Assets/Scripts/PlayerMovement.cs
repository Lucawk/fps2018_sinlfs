﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerMovement : MonoBehaviour {

    private CharacterController controller;
    private Collider collider1;
    private Vector2 axis;
    public float speed;
    public Vector3 moveDirection;
    private float forceToGround = Physics.gravity.y;

    public float jumpSpeed;
    private bool jump;
    public float gravityMagnitude = 1.0f;

    void Start ()
    {
        controller = GetComponent<CharacterController>();
        collider1 = GetComponent<CapsuleCollider>();
    }

    void Update ()
    {
        if(controller.isGrounded && !jump)
        {
            moveDirection.y = forceToGround;
        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }

        Vector3 transformDirection = axis.x * transform.right + axis.y * transform.forward;

        moveDirection.x = transformDirection.x * speed;
        moveDirection.z = transformDirection.z * speed;

        controller.Move(moveDirection * Time.deltaTime);
    }
void OnTriggerEnter(Collider other) {

		if(other.gameObject.tag == "Enemy") {
            StartCoroutine(PlayerDeath());
		}

        if(other.gameObject.tag == "Win") {
			 SceneManager.LoadScene("GameWin");
		}

	}
    public void SetAxis(Vector2 inputAxis)
    {
        axis = inputAxis;
    }

    public void StartJump()
    {
        if(!controller.isGrounded) return;

        moveDirection.y = jumpSpeed;
        jump = true;
    }

        IEnumerator PlayerDeath(){
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("GameOver");
    }


}

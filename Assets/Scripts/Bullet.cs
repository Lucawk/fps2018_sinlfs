﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    
    public GameObject bullet_prefab;
    //[SerializeField] ParticleSystem ps;
    float bulletImpulse = 20f;

    
    // Update is called once per frame
    public void Shoot () {
        //ps.Play();
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfetyManager : MonoBehaviour
{
    [SerializeField] GameObject[] Formacion;
    public float timeLaunchEnemy;
    private float currentTime =0;
    private float altura=0;
    private float ancho=0;

    private int FormacionActual;
    

    // Update is called once per frame
    void Update()
    {

        FormacionActual= Random.Range(0,Formacion.Length);

       currentTime += Time.deltaTime;
       altura = 10;
       ancho = Random.Range(-10,10);
       if(currentTime>timeLaunchEnemy){
           currentTime =0;
           Instantiate(Formacion[FormacionActual],new Vector3(ancho,altura,-200),Quaternion.identity, this.transform);
       } 
    }
}

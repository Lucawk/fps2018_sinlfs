﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoImagen : MonoBehaviour
{
    // Start is called before the first frame update

    Vector2 speed;


    Transform graphics;

    void Awake()
    {
        speed.y = -200; 
    }

   
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);
        graphics.Rotate(0,0,100*Time.deltaTime);
    }
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this);
        }
    }   
}
